package multas.principal;

import java.text.SimpleDateFormat;
import java.util.Date;
import multas.modelo.Agente;
import multas.modelo.Categoria;
import multas.modelo.Licencia;
import multas.modelo.MotivoMulta;
import multas.modelo.OrganismoTransito;
import multas.modelo.Persona;
import multas.modelo.Vehiculo;
import multas.ui.TransitoUI;

/**
 * @author Paula
 */
public class Main {

    public static void main(String[] args) {

        OrganismoTransito organismo = new OrganismoTransito();

        try {
            organismo.registrarAgente(new Agente((short) 110000, 1234, "2016/01/01", "2016/01/01"));
            organismo.registrarVehiculo(new Vehiculo("Abcd", (short) 2001, "azul", "Audi"));
            organismo.agregarMotivoMulta(new MotivoMulta((short) 2015, "Cruce Prohibido", 50000));

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            Date fecha = sdf.parse("2016/01/01");

            organismo.registrarLicencia(new Licencia(fecha, new Persona(2345, "Juan", "Torres"), Categoria.A1));

        } catch (Exception exc) {
        }

        TransitoUI transitoUI = new TransitoUI(organismo);
        transitoUI.setVisible(true);
    }
}
