package multas.modelo;

import java.util.Date;

/**
 *
 * @author salasistemas
 */
public class Licencia {

    private Date fechaExpira;
    private Persona persona;
    private Categoria categoriaVehiculo;

    public Licencia(Date fechaExpira, Persona persona, Categoria categoriaVehiculo) {
        this.fechaExpira = fechaExpira;
        this.persona = persona;
        this.categoriaVehiculo = categoriaVehiculo;
    }

    public Licencia(Date fecha, Persona persona) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Date getFechaExpira() {
        return fechaExpira;
    }

    public Persona getPersona() {
        return persona;
    }

    public Categoria getCategoriaVehiculo() {
        return categoriaVehiculo;
    }

    public void setFechaExpira(Date fechaExpira) {
        this.fechaExpira = fechaExpira;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public void setCategoriaVehiculo(Categoria categoriaVehiculo) {
        this.categoriaVehiculo = categoriaVehiculo;
    }

    @Override
    public String toString() {
        return "Licencia{" + "fechaExpira=" + fechaExpira + ", persona=" + persona + ", categoriaVehiculo=" + categoriaVehiculo + '}';
    }

}
