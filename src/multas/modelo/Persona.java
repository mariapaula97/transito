package multas.modelo;

/**
 * @author Paula
 */
public class Persona {

    private long identificacion;
    private String nombre;
    private String apellido;

    public Persona(long identificacion, String nombre, String apellido) {
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public long getIdentificacion() {
        return identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setIdentificacion(long identificacion) throws Exception {
        if (identificacion == 0 || "".equals(Long.toString(identificacion).trim())) {
            throw new Exception("identificacion no puede estar vacio");
        }
        this.identificacion = identificacion;
    }

    public void setNombre(String nombre) throws Exception {
        if (nombre == null || "".equals(nombre.trim())) {
            throw new Exception("nombre no puede estar vacio");
        }
        this.nombre = nombre;
    }

    public void setApellido(String apellido) throws Exception {
        if (apellido == null || "".equals(apellido.trim())) {
            throw new Exception("apellido no puede estar vacio");
        }
        this.apellido = apellido;
    }

    @Override
    public String toString() {
        return "Persona{" + "identificacion=" + identificacion + ", nombre=" + nombre + ", apellido=" + apellido + '}';
    }
}
