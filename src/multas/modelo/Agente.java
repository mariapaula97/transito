package multas.modelo;

/**
 *
 * @author salasistemas
 */
public class Agente extends Persona {

    private short numeroPlaca;

    public Agente(short numeroPlaca, long identificacion, String nombre, String apellido) {
        super(identificacion, nombre, apellido);
        this.numeroPlaca = numeroPlaca;
    }

    public short getNumeroPlaca() {
        return numeroPlaca;
    }

    public void setNumeroPlaca(short numeroPlaca) throws Exception {
        if (numeroPlaca == 0 || "".equals(Short.toString(numeroPlaca).trim())) {
            throw new Exception("Numero de Placa no puede estar vacio");
        }
        this.numeroPlaca = numeroPlaca;
    }

    @Override
    public String toString() {
        return "Agente{" + "numeroPlaca=" + numeroPlaca + '}';
    }
}
