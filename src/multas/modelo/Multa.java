package multas.modelo;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author salasistemas
 */
public class Multa {

    private int valor;
    private List<MotivoMulta> motivosMulta = new LinkedList<>();
    private Agente agente;
    private Date fechaMulta;
    private Persona conductor;
    private Vehiculo vehiculoMultado;

    public Multa(int valor, Agente agente, Date fechaMulta, Persona conductor, Vehiculo vehiculoMultado) {
        this.valor = valor;
        this.agente = agente;
        this.fechaMulta = fechaMulta;
        this.conductor = conductor;
        this.vehiculoMultado = vehiculoMultado;
    }

    public int getValor() {
        return valor;
    }

    public List<MotivoMulta> getMotivosMulta() {
        return motivosMulta;
    }

    public Agente getAgente() {
        return agente;
    }

    public Date getFechaMulta() {
        return fechaMulta;
    }

    public Persona getConductor() {
        return conductor;
    }

    public Vehiculo getVehiculoMultado() {
        return vehiculoMultado;
    }

    public void setValor(int valor) throws Exception {
        if (valor == 0 || "".equals(Integer.toString(valor).trim())) {
            throw new Exception("Valor Multa no puede estar vacio");
        }
        this.valor = valor;
    }

    public void setMotivosMulta(List<MotivoMulta> motivosMulta) {
        this.motivosMulta = motivosMulta;
    }

    public void setAgente(Agente agente) throws Exception {
        if (agente == null) {
            throw new Exception("agente no puede estar vacio");
        }
        this.agente = agente;
    }

    public void setFechaMulta(Date fechaMulta) {
        this.fechaMulta = fechaMulta;
    }

    public void setConductor(Persona conductor) throws Exception {
        if (conductor == null) {
            throw new Exception("conductor no puede estar vacio");
        }
        this.conductor = conductor;
    }

    public void setVehiculoMultado(Vehiculo vehiculoMultado) throws Exception {
        if (vehiculoMultado == null) {
            throw new Exception("Vehiculo Multado no puede estar vacio");
        }
        this.vehiculoMultado = vehiculoMultado;
    }

    @Override
    public String toString() {
        return "Multa{" + "valor=" + valor + ", motivosMulta=" + motivosMulta + ", agente=" + agente + ", fechaMulta=" + fechaMulta + ", conductor=" + conductor + ", vehiculoMultado=" + vehiculoMultado + '}';
    }

    public void add(MotivoMulta motivosMulta) {
        this.motivosMulta.add(motivosMulta);
    }

    public void remove(MotivoMulta motivosMulta) {
        this.motivosMulta.remove(motivosMulta);
    }
}
