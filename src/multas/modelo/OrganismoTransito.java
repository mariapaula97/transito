package multas.modelo;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author salasistemas
 */
public class OrganismoTransito {

    private List<Licencia> licenciasExpedidas = new LinkedList<>();
    private List<Agente> agentes = new LinkedList<>();
    private List<MotivoMulta> motivosMultas = new LinkedList<>();
    private List<Multa> multas = new LinkedList<>();
    private List<Vehiculo> vehiculos = new LinkedList<>();

    public void registrarLicencia(Licencia licenciasExpedidas) {
        this.licenciasExpedidas.add(licenciasExpedidas);
    }

    public void removeLicencia(Licencia licenciasExpedidas) {
        this.licenciasExpedidas.remove(licenciasExpedidas);
    }

    public void registrarAgente(Agente agentes) {
        this.agentes.add(agentes);
    }

    public void removeAgente(Agente agentes) {
        this.agentes.remove(agentes);
    }

    public void agregarMotivoMulta(MotivoMulta motivosMultas) throws Exception {
        if (this.motivosMultas.contains(motivosMultas)) {
            throw new Exception("El Motivo de la Multa ya esta registrado");
        } else {
            this.motivosMultas.add(motivosMultas);
        }
    }

    public void removeMotivoMulta(MotivoMulta motivosMultas) {
        this.motivosMultas.remove(motivosMultas);
    }

    public void registrarMulta(Multa multas) throws Exception {
        if (this.multas.contains(multas)) {
            throw new Exception("La Multa ya esta registrada");
        } else {
            this.multas.add(multas);
        }
    }

    public void removeMulta(Multa multas) {
        this.multas.remove(multas);
    }

    public void registrarVehiculo(Vehiculo vehiculos) throws Exception {
        if (this.vehiculos.contains(vehiculos)) {
            throw new Exception("El mecanico ya esta registrado");
        } else {
            this.vehiculos.add(vehiculos);
        }
    }

    public void removeVehiculo(Vehiculo vehiculos) {
        this.vehiculos.remove(vehiculos);
    }

    public List<Licencia> findLicencia(String pattern) {
        List<Licencia> found = new LinkedList<>();
        for (Licencia licenciasExpedidas : this.licenciasExpedidas) {
            if (licenciasExpedidas.getPersona().equals(pattern)) {
                found.add(licenciasExpedidas);
            }
        }
        return found;
    }

    /*public List<Agente> findAgente(String pattern) {
        List<Agente> found = new LinkedList<>();
        for (Agente agentes : this.agentes) {
            if (agentes(Short.toString(NumeroPlaca()).contains(pattern)) {
                found.add(agentes);
            }
        }
        return found;
    }

    public List<MotivoMulta> findMotivoMulta(Short pattern) {
        List<MotivoMulta> found = new LinkedList<>();
        for (MotivoMulta motivosMultas : this.motivosMultas) {
            if (motivosMultas.toString(setCodigo).contains(pattern)) {
                found.add(motivosMultas);
            }
        }
        return found;
    }*/
    public List<Multa> findMulta(String pattern) {
        List<Multa> found = new LinkedList<>();
        for (Multa multas : this.multas) {
            if (multas.getMotivosMulta().contains(pattern)) {
                found.add(multas);
            }
        }
        return found;
    }

    public List<Vehiculo> find(String pattern) {
        List<Vehiculo> found = new LinkedList<>();
        for (Vehiculo vehiculo : this.vehiculos) {
            if (vehiculo.getPlaca().contains(pattern)) {
                found.add(vehiculo);
            }
        }
        return found;
    }

    public Agente buscarAgente(Short numeroPlaca) throws Exception {
        for (Agente agente : agentes) {
            if (agente.getNumeroPlaca() == (numeroPlaca)) {
                return agente;
            }
        }
        throw new Exception("El Agente no esta registrado");
    }

    public Licencia buscarLicencia(Date fechaExpira) throws Exception {
        for (Licencia licencia : licenciasExpedidas) {
            if (licencia.getFechaExpira() == (fechaExpira)) {
                return licencia;
            }
        }
        throw new Exception("La licencia no esta registrado");
    }

    public Vehiculo buscarVehiculo(String placa) throws Exception {
        for (Vehiculo vehiculo : vehiculos) {
            if (vehiculo.getPlaca().equalsIgnoreCase(placa)) {
                return vehiculo;
            }
        }
        throw new Exception("El vehiculo no esta registrado");
    }

    public MotivoMulta buscarMotivoMulta(Short codigo) throws Exception {
        for (MotivoMulta motivoMulta : motivosMultas) {
            if (motivoMulta.getCodigo() == (codigo)) {
                return motivoMulta;
            }
        }
        throw new Exception("El Motivo de la Multa no esta registrado");
    }

    public List<Licencia> buscarLicencias(long identificacion) throws Exception {
        LinkedList<Licencia> encontradas = new LinkedList<>();
        for (Licencia licencias : this.licenciasExpedidas) {
            if (licencias.getPersona().getIdentificacion() == identificacion) {
                encontradas.add(licencias);
            }
        }
        return encontradas;
    }
}
