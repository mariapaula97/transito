package multas.modelo;

/**
 * @author Paula
 */
public class Vehiculo {

    private String placa;
    private short modelo;
    private String color;
    private String marca;

    public Vehiculo(String placa, short modelo, String color, String marca) {
        this.placa = placa;
        this.modelo = modelo;
        this.color = color;
        this.marca = marca;
    }

    public String getPlaca() {
        return placa;
    }

    public short getModelo() {
        return modelo;
    }

    public String getColor() {
        return color;
    }

    public String getMarca() {
        return marca;
    }

    public void setPlaca(String placa) throws Exception {
        if (placa == null || "".equals(placa.trim())) {
            throw new Exception("la placa no puede estar vacia");
        }
        if (placa.length() != 6) {
            throw new Exception("El numero de la placa debe contener 6 caracteres");
        }
        this.placa = placa;
    }

    public void setModelo(short modelo) throws Exception {
        if (modelo == 0 || "".equals(Short.toString(modelo).trim())) {
            throw new Exception("el modelo no puede estar vacio");
        }
        if (1990 < modelo || modelo > 9999) {
            throw new Exception(
                    "Ingrese el modelo a partir del año 1990");
        }
        this.modelo = modelo;
    }

    public void setColor(String color) throws Exception {
        if (color == null || "".equals(color.trim())) {
            throw new Exception("el color no puede estar vacio");
        }
        this.color = color;
    }

    public void setMarca(String marca) throws Exception {
        if (marca == null || "".equals(marca.trim())) {
            throw new Exception("la marca no puede estar vacia");
        }
        this.marca = marca;
    }

    @Override
    public String toString() {
        return "Vehiculo{" + "placa=" + placa + ", modelo=" + modelo + ", color=" + color + ", marca=" + marca + '}';
    }

}
