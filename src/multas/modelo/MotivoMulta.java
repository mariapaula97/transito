package multas.modelo;

/**
 * @author Paula
 */
public class MotivoMulta {

    private short codigo;
    private String descripcion;
    private int valor;

    public MotivoMulta(short codigo, String descripcion, int valor) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.valor = valor;
    }

    public short getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getValor() {
        return valor;
    }

    public void setCodigo(short codigo) throws Exception {
        if (codigo == 0 || "".equals(Short.toString(codigo).trim())) {
            throw new Exception("Codigo Motivo Multa no puede estar vacio");
        }
        this.codigo = codigo;
    }

    public void setDescripcion(String descripcion) throws Exception {
        if (descripcion == null || "".equals(descripcion.trim())) {
            throw new Exception("Descripcion Motivo Multa no puede estar vacio");
        }
        this.descripcion = descripcion;
    }

    public void setValor(int valor) throws Exception {
        if (valor <= 0 || "".equals(Integer.toString(valor).trim())) {
            throw new Exception("Valor Motivo Multa no puede estar vacio");
        }
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "MotivoMulta{" + "codigo=" + codigo + ", descripcion=" + descripcion + ", valor=" + valor + '}';
    }
}
/*

 */
